<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Lo Ciento Papi</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="bg-gray-900">

<h1 class="text-4xl font-bold text-center text-gray-200">Saluuuuuut</h1>
  <div class="container mx-auto">
  <?php
    $filename = 'cp.csv';

    if (file_exists($filename)) {
      $file = fopen($filename, "r");
      while (($line = fgetcsv($file, 1000, ";"))!== FALSE) {
        $cp[] = $line;
      }
      fclose($file);
    } else {
      echo "El archivo no existe";
    }
    
foreach ($cp as $key => $value) {
  echo '<div class="flex items-center justify-between p-4 m-2 bg-gray-700">';
  echo '<span class="text-xl font-bold text-gray-200">' . $value[0] . '</span>';
  echo '<span class="text-gray-400">' . $value[1] . '</span>';
  echo '</div>';
}
  ?>

  </div>
</body>

</html>